## Oproep

Wij, OSP, Louis en Amelie, zijn op zoek naar 2 medebewoners in onze werkruimte. Voorlopig is de ruimte een verzamelplaats voor grafisch ontwerpers die werken met een specifieke invalshoek en aanpak op het gebied van typografie, websites, web-to print tools en plotters (zie onderstaande beschrijvingen), maar we zouden blij zijn om elk type praktijk in onze ruimte te verwelkomen. We willen permeabiliteit, dus nieuwsgierigheid of interesse in elkaars praktijken is een pluspunt. In staat zijn om nieuwe samenwerkingen te bedenken kan ook geweldig zijn, maar is geen must.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/myzZiPmfwNKQyte?x=3840&y=798&a=true&file=000022.JPG&scalingup=0">

Onze atelierruimte bevindt zich op [boulevard Pacheco 34](https://www.openstreetmap.org/way/30145405), we delen de vloer met andere kunstenaars, collectieven en verenigingen. De leden van de Meyboom artist-run spaces staan vermeld op deze webpagina <http://meyboom.osp.kitchen/>. De vloer probeert als een gemeenschap te zorgen voor onderhoud en organisatie van de vloer. De betrokkenheid bij het collectieve leven van de vloer kan naar eigen wens worden vormgegeven, maar er moet wel rekening mee worden gehouden. 

Wij spreken Engels, Frans en Nederlands, met Engels als gemene deler.

**Maandelijkse huur** 
250 euro voor 2 bureaus, of 125 euro per persoon. We bespreken het liever tijdens een bezoek, om rekening te kunnen houden met uw ruimtebehoeften, en de herinrichting van de ruimte.
**Beschikbare ruimte** 
2 "bureaus", in een gedeelde studio op de tweede verdieping.
**Inbegrepen in de kamer** 
vergadertafel, printer, scanner, pennenplotters, let op: de vloer is bedekt met tapijt.
**Inbegrepen op de verdieping** 
vergadertafel, printer, scanner, pennenplotters, let op: de vloer is bedekt met tapijt.
Mogelijkheid tot het reserveren van vergaderzalen (capaciteit tussen 10 en 60 personen) op de begane grond (gedeeld met het hele gebouw).
**Andere diensten en logies inbegrepen** 
parkeerplaats (auto en fiets) achter een gesloten deur, lift, internet, verwarming
**Installation** vanaf juli. (Ook mogelijk in september)

Als u geïnteresseerd bent (voor één of twee personen), stuur ons dan een e-mail met een paragraaf met uw wensen en een kleine presentatie van uw activiteiten naar <a href="mailto:miam@osp.kitchen">miam@osp.kitchen</a>.

## Bios

**Open Source Publishing** <http://osp.kitchen> bevraagt de invloed en de betaalbaarheid van digitale tools door haar praktijk van (in opdracht) grafisch ontwerp, pedagogie en toegepast onderzoek. Ze gebruiken bij voorkeur uitsluitend vrije en open source software (F/LOSS). Door middel van haar projecten bevraagt OSP de wijze van samenwerking tussen grafisch ontwerpers en kunstenaars, tussen grafisch ontwerpers en culturele instellingen.
In de OSP groep dagelijks op locatie: Einar Andersen, Gijs de Heij, Ludi Loiseau, Sarah Magnan, Doriane Timmermans.

**Amélie Dumont** <https://www.amelie.tools/> is grafisch ontwerper, typograaf en ontwikkelaar. Ze kookt aangepaste tools voor publicatie via webpagina's en drukwerkdragers of ontwerpt lettertypes. Amélie heeft ook ervaring in het geven van web-to-print workshops in kunstscholen.

**Louis Garrido** <https://www.instagram.com/louloularouille/> is grafisch ontwerper en typograaf. Hij beoefent inclusief vectorontwerp in verschillende vormen: als onderdeel van uitgeefprojecten, identiteitsopdrachten of zelfpublicatie om ingekleurd te worden, op bolvormen van petanqueballen en via zijn omwegen op vlooienmarkten. Louis geeft ook workshops en interventies in kunstscholen.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/wxJdppLXk9fy8Sy?x=3840&y=798&a=true&file=2022-06-22_11-39-54.jpg&scalingup=0">

Alle 3 de entiteiten hebben een bijzondere belangstelling voor typografie, websites, web-to-print tools en plotters. 
Een ruimte van het atelier is speciaal ingericht om de praktijk van een plotterscollectie (overtrektafels) te huisvesten en het verblijf van eventuele gasten mogelijk te maken.
Naast de werktuigen hebben verschillende leden nog andere aanknopingspunten. Amélie, Doriane, Louis, Ludi en Sarah hebben ook gemeen dat ze op verschillende plaatsen in het mediaproject Médor werken. Louis en Ludi zijn actieve leden van het collectief Bye Bye Binary.
Andere gelinkte projets zijn Algolit en Just for the record.