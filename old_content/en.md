## Call

We, OSP, Louis and Amelie, are looking for 2 cohabitants in our workspace. For now the room is gathering graphic designers working with a specific angle and approach on typography, websites, web-to print tools and plotters (see below descriptions), but we would be happy to welcome any type of practice in our space. We wish to have permeabilities so a curiosity or interest in the practices of each other is a plus. To be able to envision new collaborations could be also great but not a must.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/myzZiPmfwNKQyte?x=3840&y=798&a=true&file=000022.JPG&scalingup=0">

Our workshop space is situated on [boulevard Pacheco 34](https://www.openstreetmap.org/way/30145405), we share the floor with other artists, collectives and associations. The members of the Meyboom artist-run spaces are listed on this webpage <http://meyboom.osp.kitchen/>. The floor tries to take care of maintenance and organization of the floor as a community. The involvement in the collective life of the floor can be shaped according to your wishes but it is to be taken into account. 

We speak English, French and Dutch, with English as the common denominator.

**Monthly rent** 250 euros for 2 desks, or 125 euros per person. We prefer to discuss it during a visit, in order to take in account your space needs, and the rearrengement of the space.
**Available space** 2 "desks", in a shared studio on the second floor.
**Included in the room** meeting table, printer, scanner, pen plotters, note that the floor is covered in carpet.
**Included on the floor** kitchen, and communal space shared with other members of the floor. 
Possibility to book meeting rooms (capacity between 10 and 60 people) on the ground floor (shared with the whole building).
**Other services and accomodations included** parking space (car and bike) behind a locked door, lift, internet, heating
**Installation** from July. (Also possible in September)

If you are intersted (either for one or two people), please send us an email with a paragraph including your wishes and small presentation of you activities at <a href="mailto:miam@osp.kitchen">miam@osp.kitchen</a>.

## Bios

**Open Source Publishing** <http://osp.kitchen> questions the influence and affordance of digital tools through its practice of (commissioned) graphic design, pedagogy and applied research. They prefer to use exclusively free and open source softwares (F/LOSS). Through its projects OSP questions the modes of collaboration between graphic designers and artists, between graphic designers and cultural institutions.
In the OSP group on site daily: Einar Andersen, Gijs de Heij, Ludi Loiseau, Sarah Magnan, Doriane Timmermans.

**Amélie Dumont** <https://www.amelie.tools/> is a graphic designer, typographer and developer. She cooks custom tools for publishing through webpages and print supports or designing fonts. Amélie also has experience in giving web-to-print workshops in art schools.

**Louis Garrido** <https://www.instagram.com/louloularouille/> is a graphic designer and typographer. He practices inclusive vector design in several forms: as part of publishing projects, identity commissions or self-publishing to be colored, on spherical shapes of petanque balls and through his detours in flea markets. Louis also drives workshops and interventions in art schools.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/wxJdppLXk9fy8Sy?x=3840&y=798&a=true&file=2022-06-22_11-39-54.jpg&scalingup=0">

All 3 entities have a particular interest in typography, websites, web-to-print tools and plotters. 
A space of the workshop is specially arranged to accommodate the practice of a plotters collection (tracing tables) and allow the residence of possible guests.
Beyond the tools, several members have other connection points. Amélie, Doriane, Louis, Ludi and Sarah also have in common that they work at different places in the Médor media project. Louis and Ludi are active members of the Bye Bye Binary collective.
Other linked projets are Algolit and Just for the record.