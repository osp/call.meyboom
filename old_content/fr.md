## Appel

OSP, Louis et Amélie sont à la recherche de 2 cohabitant·es d'atelier. Pour l'instant, la salle rassemble des graphistes travaillant avec un angle et une approche spécifiques sur la typographie, les sites web, les outils web-to print, et les plotters (voir les descriptions ci-dessous), mais nous serions heureux d'accueillir toute autre type de pratique dans notre espace. Nous souhaitons avoir des perméabilités, donc une curiosité ou un intérêt pour les pratiques des un·es et des autres serait un plus. Être capable d'envisager des collaborations serait également une bonne chose, mais ce n'est pas une obligation.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/myzZiPmfwNKQyte?x=3840&y=798&a=true&file=000022.JPG&scalingup=0">

L'atelier se situe au [numéro 34 sur le boulevard Pacheco](https://www.openstreetmap.org/way/30145405), on partage l'étage avec d'autres artistes, collectifs ou association. Les membres du Meyboom artist-run spaces sont repris sur cette page web <http://meyboom.osp.kitchen/>. L'étage essaie de gèrer sa maintenance et son organisation comme une communauté. L'implication dans la vie collective de l'étage peut être minimal ou plus importante selon vos envies mais elle est à prendre en compte.

Nous parlons anglais, français et néerlandais, avec l'anglais comme dénominateur commun.

**Loyers mensuels** 250 euros pour 2 bureaux, soit 125 euros par personne. Nous préférons en discuter lors d'une visite, pour pouvoir prendre en compte vos besoins d'espaces et le réarrengement de l'espace.
**Espace disponible** 2 "bureaux", dans un studio partagé au deuxième étage.
**Inclu dans le studio** table de réunion, imprimante, scanner, traceur, à noter: le sol est couvert de moquette.
**Inclu à l'étage** cuisine, espace commun à partager avec les autres membres de l'étage.
Possibilité de réserver des espaces de réunions (capacité entre 10 et 60 personnes) au rez-de-chaussé (partager avec tout l'étage).
**Services et accomodations comprises** espace parking (voiture et vélo) fermé à clé, ascenseur, internet, chauffage
**Installation** Possible dès juillet (aussi possible en septembre)

Si vous êtes intéressé·es (soit pour une ou deux personnes), envoyez-nous un mail avec un paragraphe de vos envies et une petite présentation de vos activités à <a href="mailto:miam@osp.kitchen">miam@osp.kitchen</a>.

## Bios

**Open Source Publishing** <http://osp.kitchen> questionne l'influence et l'affordance des outils numériques à travers sa pratique de design graphique (commisionné), la pédagogie, et la recherche appliquée. Iels préfère utiliser exclusivement des logiciels libre et open-source (F/LOSS). À travers ses projets, OSP questionne les modes de collaboration entre designers graphique et artistes, entre designers graphique et institution culturel.
Dans le groupe OSP sur place au quotidien : Einar Andersen, Gijs de Heij, Ludi Loiseau, Sarah Magnan, Doriane Timmermans.

**Amélie Dumont** <https://www.amelie.tools/> est une graphiste, typographe et développeuse. Elle conçoit des outils personnalisés pour la publication à travers des pages web et des supports d'impression ou la conception de polices de caractères. Amélie a également de l'expérience dans l'animation d'ateliers web-to-print dans les écoles d'art.

**Louis Garrido** <https://www.instagram.com/louloularouille/> est graphiste typographe. Il pratique le dessin vectoriel inclusif sous plusieurs formes : Dans le cadre de projet d'édition, commandes d'identité ou auto éditions à colorier, sur formes sphériques en pétanque et à travers ses détours de brocantes. Louis anime aussi des formats workshop et interventions en écoles d'art.

<img src="https://cloud.osp.kitchen/apps/files_sharing/publicpreview/wxJdppLXk9fy8Sy?x=3840&y=798&a=true&file=2022-06-22_11-39-54.jpg&scalingup=0">

Toustes 3 entités portent un intérêt particulier à la typographie, aux sites web, aux outils web-to-print et tables traçantes. 
Un espace de l'atelier est aménagé spéfiquement pour accueillir la pratique de la collection de plotters (tables tracantes en place) et permettent l'acceuil de résident·es temporaires.
Au delà des outils, plusieurs membres ont d'autres points de connexion. Amélie, Doriane, Louis, Ludi et Sarah ont aussi pour point commun de travailler à différents endroits du projet média Médor. Louis et Ludi sont particules actives de la collective Bye Bye Binary.
D'autres projets liés sont Algolit et Just for the record.