
<!--[call]-->

[TOC]

# Appel à OSPs

Nous, Open Source Publishing (OSP), invitons des personnes **à rejoindre notre collectif**.

[OSP](https://osp.kitchen) est un collectif de graphistes basé à Bruxelles. Nos matérieux sont la typographie, les sites web, les outils web-to print et les traceurs. Nous questionnons l’influence et les possibilités des outils numériques à travers la pratique du graphisme (de commande), la pédagogie et la recherche appliquée. Nous utilisons exclusivement des logiciels libres et open source (F/LOSS). À travers nos projets, nous questionnons les logiciels en tant qu’objets culturels et les modes de collaboration entre graphistes, artistes, institutions culturelles et écoles. 

![A vintage looking picture of all the collective members: (from left to right) Amélie, Sarah, Ludi, Einar, Doriane, Gijs.](media/us-2.jpg)


<!-- Nous avons également publié un appel à résidence au sein d’OSP [ici](). -->

<!-- (outils et interfaces) -->

## Situation actuelle

Nous activons cette année un virage au sein de notre pratique avec la volonté d’aménager plus d’espaces dédiés à des projets de recherche. Nous cherchons un équilibre avec ceux jusqu’ici principalement basés sur la commande. Dans le cadre de ce changement, nous aimerions accueillir de nouveaux membres avec leurs propres expériences et expertises. Nous souhaitons faire de la place aux chercheur·ses, artistes, designers, étudiant·es et/ou amateur·ices qui s’intéressent à notre pratique et qui souhaiteraient établir un pont avec la leur.

Au sein d’OSP, les membres travaillent actuellement sur :

* les plates-formes d’édition collaborative ;
* les processus de publication du web à l’impression ;
* les polices de caractères et les licences typographiques ;
* les spécificités des normes web (HTML/CSS) comme médium.

Ces chantiers sont activés par le biais de la commande, de la recherche et de la pédagogie. 
En dehors d’OSP, nos pratiques incluent également la littérature algorithmique, la typographie inclusive, le cyberféminisme, le codage textile et le dj-ing. Nous collaborons avec des institutions culturelles, des curateur·ices, des artistes, des architectes, des journalistes et des chercheur·euses.
Nous concevons et développons des sites web, des livres et des identités, et organisons des workshops. Nous sommes régulièrement sollicités pour des interviews ou des contributions en tant que chercheur·euses en raison de notre expérience singulière des outils et de la culture open-source, et de part notre relation sensible avec les outils.

En tant que collectif, OSP a des contours irréguliers. Presque tous les membres ont une pratique parallèle à leur travail au sein d’OSP. L’implication OSP de chacun·e varie de un à trois jours par semaine.  <A côté de cela, nous avons notre propre pratique en matière de conception, de code et d’éducation. -->
Avec cet appel, nous souhaitons rendre plus transparent le processus d’adhésion ou de collaboration avec le groupe.

## Appel à membres

Nous recherchons **deux nouveaux membres qui peuvent apporter de nouvelles expériences et pratiques**. Nous aimons être surpris·es, donc vous êtes invité·es à postuler même si vous n’êtes pas un·e graphiste ou un·e développeur·euse. Bien sûr, les questions autour de la culture libre, du design numérique, des interfaces ou des outils se présentent un dénominateur commun. Votre implication dans OSP peut prendre différentes formes en fonction de vos souhaits et de vos besoins. <Par exemple, aucun des membres actuels n’est impliqué à plein temps dans le collectif. -->

Comme nous sommes dans un processus de diversification et **d’investigation des pistes par lesquelles la recherche peut reprendre place de notre pratique quotidienne d’OSP**, nous sommes particulièrement intéressé·es par des personnes ayant des expériences sur la façon de faciliter ce processus ou d’explorer de nouvelles façons de faire.<!-- Lorsque l’occasion se présente, les membres peuvent unir leurs forces pour demander des subventions pour des projets de recherche spécifiques.-->
L’aspect collaboratif de notre travail est fondamental et constitue une grande partie de notre pratique. Les projets nous parviennent souvent par l’intermédiaire des membres individuels. Nous vous invitons à apporter votre propre pratique et votre écosystème pour étendre le collectif.

Avec cet appel, nous recherchons des "membres effectif·ves". C'est à dire des membres qui sont prêt·es à :

* démontrer une capacité à travailler en collaboration (et non en solitaire) ;
* montrer une capacité à étendre et à approfondir sa pratique par la recherche (artistique ou académique) ;
* utiliser des logiciels libres, dans le cadre de l’association, en totalité ou en partie du processus ;
* publier des fichiers sources sous licence libre ;
<!-- * avoir lu : Statuts de l’OSP asbl, [Accord de collaboration de l’OSP asbl](#) et [Recettes de l’ordre interne](#) jusqu’à la fin. -->

Concrètement, nous vous invitons à :

* vous joindre à nous pour des travaux de commande **et** des projets de recherche ;
* contribuer activement à OSP en tant que collectif, notamment en participant aux réunions hebdomadaires ;
* travailler dans notre studio selon vos disponibilités.

Nous nous rémunérons par projet, en utilisant OSP comme structure de facturation. Une partie des revenus (25%) est utilisée pour payer les coûts (loyer, voyages, hébergements, matériel, livres, etc.), pour soutenir le travail interne (gestion, autoédition) et pour créer une cagnotte commune<!--financière-->. 
Le rythme et la nature des projets OSP étant très fluctuants, nous ne pouvons pas assurer un revenu régulier et suffisant par le biais des travaux de commande. Nous avons récemment commencé à expérimenter la possibilité de fournir un revenu plus régulier et prévisible via ce budget de réserve.


## Processus de candidature

Nous vous demandons de soumettre une proposition, dont la forme peut dépendre de votre pratique.
Nous espérons que votre proposition pourra nous éclairer sur les questions suivantes : 

* Quelle est votre **pratique actuelle** ?
* Quelle a été votre **trajectoire** jusqu’à présent ?
* Le cas échéant, les **pistes de recherche** qui vous intéressent et des idées sur la manière de les poursuivre au sein d’OSP.
* Si vous postulez en tant que non-graphiste : précisez comment vous **imaginez la collaboration** avec les membres actuels d’OSP (sur commande et/ou dans des projets de recherche) ?

Indication de longueur : environ 1500 mots.
Le document peut être présenté dans le format de votre choix : pdf, site web, fichier texte brut, vidéo, son ; il peut être envoyé par courrier électronique en tant que document commun ou être hébergé (dans ce cas, il suffit de nous envoyer le lien).

Si cela est pertinent, nous vous demandons également d’inclure la présentation de projets ou expériences récentes (5 maximum).
Cela peut prendre la forme d’un portfolio visuel ou de courts textes expliquant les différents projets sur lesquels vous avez travaillé.

Cette appel a été publiée pour la première fois le 1<sup>er</sup> juin 2023.
Les candidatures doivent être envoyées à <cookwithus@osp.kitchen>.
La date limite pour nous faire parvenir une proposition est le 30 juin.
Vous recevrez une réponse automatique pour en confirmer la réception. 
Nous vous répondrons pour le 12 juillet et organiserons ensuite des rencontres sur juillet et septembre en fonction de vos disponibilités.

Si vous avez des questions spécifiques concernant cet appel, n’hésitez pas à nous contacter à < miam@osp.kitchen>.

## Ce que nous offrons

### Au studio

Cette liste n’est pas exhaustive et vient avec l’environnement socio-technique complexe qu’est un studio.

* Un bureau
* Une table de réunion pouvant accueillir jusqu’à 8 personnes
* Une imprimante laser, couleur A3
* Une collection de traceurs, avec des tailles d’impression allant de A∞ → A0
* Deux machines de découpe Silhouette Caméo.
* Des petites imprimantes thermiques
* Un projecteur, un scanner A3, des échantillons papiers et couleurs, etc.

![A vintage looking picture of the building whe the current studio is, the Meyboom.](media/meyboom.jpg)


Le studio d’OSP est situé au 2<sup>e</sup> étage d’un bâtiment équipé d’ascenseurs, l’une des portes d’accès est munie d’une rampe. L’étage dispose de toilettes avec lavabos accessibles aux fauteuils roulants sans main courante. Les transports publics sont à quelques centaines de mètres (Botanique/Kruidtuin, Rogier, Gare centrale, Bruxelles-Congrès). L’immeuble dispose de racks à vélos dans un parking privé en sous-sol.

<!-- ![Looking at the North tower](media/window-view.jpg) -->


### Travailler à plusieurs

OSP est composé principalement de francophones et de néerlandophones, mais la plupart des communications au sein d’OSP se font en lingua franca anglaise.

Notre atelier fait notamment partie du [Meyboom artist-run-spaces](http://meyboom.osp.kitchen). L’étage dispose d’une cuisine commune et d’espaces de réunion. Meyboom rassemble une communauté d’individus et de collectifs travaillant dans différents domaines. Un groupe qui évolue depuis sa création en 2013. Anciennement situé dans la Vaartstraat, World Trade Center 1, puis dans l’ancien siège d’Actiris au centre ville, nous habitons maintenant le deuxième niveau du bâtiment Pacheco 34, situé sur le Boulevard Pachécolaan 34 entre la gare centrale et la gare du Nord dans la ville de Bruxelles - un espace que nous aménagons de manière durable pour les membres du Meyboom artist-run-spaces.


### Partage des connaissances

OSP a **16 ans de pratique** dans la création graphique **avec des outils F/LOSS**.
En chemin, nous avons essayé de nombreux outils et acquis une collection variée d’expertises, y compris :

* une connaissance approfondie de la culture du logiciel libre, de ses licences et de ses outils ;
* des connaissances en programmation (HTML, CSS, JS et Python) et en développement d’outils ;
* le dessin et les torsions de caractères ;
* des outils pour rendre les PDF agiles : réorganiser, imposer, compresser et changer l’espace colorimétrique ;
* une expertise (typo)graphique et visuelle qui prend en compte les moyens de production.

Le réapprentissage constant est important pour nous, et nous voyons les projets comme des opportunités d’apprendre les fragiles artisanats des autres, particulièrement dans le vaste écosystème technique que sont les outils F/LOSS.

Vous trouverez sous les titres suivants certains de nos points d’entrée dans la recherche :
[Ecotones of collaboration](https://ecotones.caveat.be/), [Up pen down](http://osp.kitchen/live/up-pen-down/), [Drawing Curved](http://drawingcurved.osp.kitchen/foreword.xhtml), [Déclarations](https://declarations.ungual.digital/). 


<iframe src="https://player.vimeo.com/video/286661261?h=e887a61584" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

[Up pen down, page 5, Ductus](https://vimeo.com/286661261)   

## Membres actuels

Les membres actuels actifs au quotidien :

**Einar Andersen** est un designer, développeur entre autres choses. Ces jours-ci, Einar travaille principalement sur le design et le développement web, mais il passe son temps libre à programmer de l’art génératif pour les traceurs, à explorer des flux de travail alternatifs en design graphique et des claviers bizarres.

**Amélie Dumont** (she, elle) est graphiste, typographe et développeuse. Elle conçoit des outils sur mesure pour la publication de pages web et de supports imprimés et a également une pratique non-conventionnelle de la création de caractères. Amélie est également enseignante à l’erg et a l’expérience de l’animation d’ateliers web-to-print dans des écoles d’art. Elle a rejoint OSP en septembre 2022 et découvre encore ce que c’est que de travailler en tant que collectif.

**Gijs de Heij** est graphiste, développeur et artiste. Au sein d’OSP, il travaille sur les processus de web-to-print et les plateformes de publication. Il s’intéresse également aux traceurs à plume. Au sein du groupe de recherche Algolit, il travaille sur la littérature algorithmique.

**Ludi Loiseau** (she, elle) enseigne la typographie et les cultures numériques à l’[erg](https://wiki.erg.be). Elle est cofondatrice du magazine d’investigation belge [Médor](medor.coop) où elle est tour à tour pilote visuel, metteur en page et membre du conseil d’administration. En 2018, Ludi a co-initié la collective [Bye Bye Binary](typotheque.genderfluid.space) qui propose d’explorer de nouvelles formes graphiques et typographiques en prenant l’écriture inclusive comme terrain de recherche. Elle a récemment rejoint la collective Poxcat qui soutien les WMXMUSIC Parties, DJ mixes & Radio Shows à Bruxelles.

**Sarah Magnan** (she, elle) est graphiste au sein d’OSP et participe à plusieurs pratiques collectives au sein d’autres collectifs à l’intersection entre féminisme, stratégies décoloniales, logiciels libres et outils de connaissance en ligne. Elle est co-fondatrice de Médor et de l’asbl féministe (irl et url) Just For The Record. Elle travaille à mi-temps à La Cambre ENSAV, accompagnant à une transition vers des outils open source, facilitant les pratiques de soins et d’inclusivité. 


**Doriane Timmermans** (she, elle) est développeuse, designer et artiste, avec un intérêt particulier pour les CSS et l’artisanat de la conception avec le langage.
Elle aime considérer les systèmes et les processus automatisés comme des supports sensibles. Elle aime créer des outils aux formes étranges qui remettent en question nos façons de faire.

## Membres satellites

Plusieurs membres nous rejoignent occasionnellement sur les projets : Antoine Gelgon, Pierre Huyghebaert, Alexandre Leray.

<!--[faq]-->

Progressivelly filling this faq as we get questions.

### "are these memberships limited to people only living in Brussels or very nearby?"

This call is not limited to people living in Brussels, though we've noticed that it is important for us to regularly meet in the same space. We all have different rythms and involvements and several options are possible, but we do think that for a new collaboration, a minimum of 2 days a week, physically in the studio, is needed to give a shape and make space for this new relationship. However, we do understand that members might have other obligations through the year (residencies, other works) that doesn't allow them to be there physically the whole year.

In short, we prefer meeting in person and think at least two days a week would be best.

<!--[modal]-->


## Un mot sur les fontes squelettes en rendu css

Nous avons profité de cet appel pour continuer d’expérimenter autour de la typographie “single-line” en dessinant ici la typographie de la page web par le ductus, c’est à dire par le squelette des caractères et non par leur contour.

Le principal obstacle à ce processus est un problème d’encodage. Pour utiliser une vraie fonte à ligne unique (c’est-à-dire une fonte ou les caractères sont des tracés sans épaisseur), les chemins constituant les glyphes sont ouverts (c’est-à-dire qu’ils ne forment pas une boucle. Pensez à un S par exemple : les deux extrémités ne se rejoignent pas, alors que, dans un O, elles le feraient). Et malheureusement la plupart des formats de fontes ne supportent pas les chemins ouverts.

Il est possible d’encoder des chemins ouverts dans une fonte en utilisant le langage SVG. Cela peut se faire par le biais de polices SVG, qui ne sont actuellement prises en charge que par Safari, ou par le biais de la table SVG dans une police OpenType, prise en charge par Firefox, Safari et Edge, mais explicitement rejetée par Chrome. Cependant, il y a peut-être de l’espoir dans cette voie grâce aux emojii :

>"Le format de police SVG - en tant que format de police autonome - est essentiellement obsolète. Plus ou moins personne n’utilise les polices SVG autonomes. Cependant, les polices SVG sont étonnamment vivantes, grâce aux emojis. Le format de police SVG a été intégré à OpenType, et les polices OpenType peuvent contenir une "table de polices SVG", qui est simplement une police construite selon le format de police SVG 1.1. Les polices de cette spécification sont appelées polices "OTF+SVG" et ont généralement pour but, lorsque le logiciel client ne peut pas afficher un caractère donné dans la table SVG, de se rabattre sur l’affichage d’une alternative TTF. Ce qui est intéressant avec OTF+SVG, c’est qu’en plus d’avoir des caractères à base de traits, il peut également contenir des informations de couleur - c’est pourquoi ce format est parfois appelé "police de couleur". OTF+SVG est pris en charge par Photoshop, Illustrator, Firefox, MS Edge, Pages, Keynote, TextEdit, etc. C’est pourquoi les polices SVG existent toujours, même si elles ne sont pas souvent observées en tant que telles".
>
> &mdash; from [extrait de hershey-text-v30](https://www.evilmadscientist.com/2019/hershey-text-v30/)

Cette approche a été explorée par l'isdaT dans leur [SingleLine_otf-svgMaker](https://github.com/isdat-type/SingleLine_otf-svgMaker).

>"Les polices OpenType-SVG à chemins ouverts qui en résultent comprennent deux couches : une couche à ligne unique basée sur le SVG et une seconde couche avec un OTF à contours classiques. Par défaut, si une application ne peut pas utiliser la couche SVG à chemins ouverts, elle devrait basculer automatiquement vers la couche OTF à contours, également intégrée dans ces polices OpenType-SVG."


## L’artisanat

Ce qui se passe sur cette page est différent. 

Nous utilisons ici un hack développé par des personnes ayant besoin de polices de caractères pour des machines de découpe laser et cnc et qui échangent leurs polices en ligne. L’astuce consiste à transformer des chemins ouverts en **chemins artificiellement fermés**. Cela se fait en doublant le chemin, en inversant la direction de la copie et en connectant le chemin original et sa copie inversée. Au final, chaque point est doublé, à l’exception des extrémités. Cela permet d’exporter une police à double ligne d’épaisseur zéro, qui se comporte en grande partie comme une vraie police à une ligne unique à cause de la superposition exacte des deux chemins.

Pour ce site web, nous avons utilisé un [script python](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf), utilisant le [module python fontforge](https://fontforge.org/docs/scripting/python.html), pour convertir n’importe quelle **vraie police SVG simple ligne** en **police OTF double ligne**, permettant ainsi une tentative d'utilisation de polices “single-line” sur le web.

Heureusement, des polices SVG libres à simple ligne existent déjà, grâce aux utilisations CNC. 
Il y a notamment l’extension inkscape **Hershey text**, pour le dessin avec traceurs (plotters), qui contient plusieurs variantes des polices historiques **Hershey**, utilisées ici. D’autres exemples peuvent être trouvés sur les forums et guides de fablabs.

## CSS

Une fois la police générée, le trait réel est dessiné dans le navigateur lui-même avec les déclarations CSS, en utilisant la propriété [`-webkit-text-stroke`](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke).

Cette propriété est prise en charge par la plupart des navigateurs et incluse dans le [Living standard](https://compat.spec.whatwg.org/#the-webkit-text-stroke), mais ne fait pas encore partie d’une norme CSS. Cela implique une grande fragilité, et rend cette propriété inutilisable pour des pages web accessibles. Nous avons donc profité de l’occasion pour étendre l’expérience avec ici un toogle de bascule **trait | contour**, une occasion de montrer la différence des processus et d'accompagner vos yeux. Notons que, plus spécifiquement:

* Chrome crée des terminals carré et Firefox des ronds, laissant les règles d'interpretation du tracé entièrement au navigateur.
* certaine lettres peuvent disparaître, et certaines courbes apparaissant comme doublées a certain niveau de zoom seulement, nous avons aucune idée de pourquoi, mais évidemment `-webkit-text-stroke` n'as jamais été pensé pour un pareil usage.

Cette approche particulière a cependant d’autres conséquences agréables, que les amateurs de polices de caractères ont rêvé de voir possibles sur le web.

Parce que `-webkit-text-stroke` supporte les **unités globales** (c’est-à-dire des unités indépendantes de celle de la taille de police), nous pouvons :

* avoir la même largeur de trait pour des éléments de texte ayant des tailles de police différentes (puisque la largeur du trait devient indépendante de la taille de la police)
* définir une même largeur de trait pour des familles de polices différentes
* synchroniser une même largeur de trait avec d’autres éléments HTML ou SVG (la bordure d’une `div`, le contour d’un dessin SVG, etc...)
* il existe déjà plusieurs polices libres accessibles pour cela sur le web : de vraies polices SVG à ligne unique issues du monde des logiciels libres CNC.


## Fragilité

Cette expérience peut être considérée comme croisant plusieurs domaines d’intérêt d’OSP, tels que :

* Les paradigmes de typographie “stroke” (trait) contre “outline”  (contour)
* CSS et le grain des supports web
* Les frictions avec les standards ouverts
* Comment les outils façonnent les pratiques