
<!--[call]-->

[TOC]

# OSP oproep voor nieuwe leden

Wij, Open Source Publishing (OSP), zijn op zoek naar mensen die zich bij ons collectief willen aansluiten. 

[OSP](https://osp.kitchen) is een collectief voor grafisch ontwerp in Brussel. We werken aan en met typografie, websites, web-to print tools en plotters. We gebruiken uitsluitend vrije en open source software (F/LOSS, Free/libre open source software). Middels onze projecten bevragen we software als culturele objecten en vormen van samenwerking tussen grafisch ontwerpers, kunstenaars, culturele instellingen en scholen. We onderzoeken de invloed en de mogelijkheden van digitale hulpmiddelen door middel van grafisch ontwerp (al dan niet in opdracht), onderwijs en toegepast onderzoek.

![A vintage looking picture of all the collective members: (from left to right) Amélie, Sarah, Ludi, Einar, Doriane, Gijs.](media/us-2.jpg)

## Waar staat OSP nu? 

Momenteel **vergroten we de rol van op onderzoek gebaseerde projecten**, waar onze praktijk voorheen voornamelijk op opdrachten gebaseerd was. Als onderdeel van deze verschuiving willen we graag nieuwe leden verwelkomen met hun eigen expertise en ervaringen. We maken ruimte voor onderzoekers, kunstenaars, ontwerpers, studenten en/of amateurs die interesse hebben in onze praktijk en daar hun eigen praktijk aan willen verbinden.

Binnen OSP werken de leden momenteel aan:

* gezamenlijke publicatieplatforms;
* web-to-print publicatieprocessen;
* lijn-lettertypes en licenties;
* specifieke kenmerken van webstandaarden (HTML/CSS) als medium.

We doen dit door middel van opdrachten, onderzoek en onderwijs. Buiten OSP omvatten onze praktijken ook algoritmische literatuur, inclusieve typografie, cyberfeminisme, textielcodering en dj-en. We werken samen met culturele instellingen, curatoren, kunstenaars, architecten, journalisten en onderzoekers. We ontwerpen en ontwikkelen websites, boeken en identiteiten, en organiseren workshops. Van tijd tot tijd worden we gevraagd voor interviews of bijdragen als onderzoekers vanwege onze bijzondere ervaring met open-source tools en cultuur, en onze bewuste relatie met de tools die we gebruiken.

Als collectiviteit heeft OSP rafelranden. Bijna alle leden hebben een praktijk naast hun werk binnen OSP: de betrokkenheid varieert van één tot drie dagen per week. Met deze oproep willen we de procedures om lid te worden van, of samen te werken met OSP, transparanter maken.

## Oproep voor nieuwe leden

We zijn op zoek naar **twee nieuwe leden die nieuwe ervaringen en praktijken kunnen inbrengen**. Dat wil zeggen, we laten ons graag verrassen, dus je bent ook zeker welkom om te solliciteren als je geen grafisch ontwerper of ontwikkelaar bent. Echter, vragen rond vrije cultuur, digitaal ontwerp, interfaces of tools kunnen een gemene deler zijn. Je betrokkenheid bij OSP kan verschillende vormen aannemen, afhankelijk van je wensen en behoeften.

We bevinden ons in een proces van diversificatie, en testen uit hoe onderzoek naast onze dagelijkse OSP-praktijken kan leven. Daarom zijn we vooral geïnteresseerd in mensen die ervaring hebben met het faciliteren van dit soort processen of met het verkennen van nieuwe manieren om samen te werken met vergelijkbare uiteenlopende praktijken. 

Samenwerking is fundamenteel voor ons en een groot deel van onze praktijk. Projecten komen vaak binnen via de individuele leden. Je bent vrij om je eigen praktijk en ecosysteem in te brengen om zo het collectief uit te breiden.

Samenvattend zijn we op zoek naar leden die:

* blijk geven samen te kunnen werken;
* in staat zijn om hun praktijk uit te breiden en te verdiepen door middel van onderzoek (artistiek of academisch);
* vrije en open source software gebruiken, wanneer mogelijk; 
* bronbestanden onder vrije licenties publiceren.

Concreet stellen we ons voor dat je:

* deelneemt aan opdrachten en/of onderzoeksprojecten;
* actief bijdraagt aan OSP als collectief, met name door deel te nemen aan wekelijkse vergaderingen;
* in onze atelierruimte werkt volgens jouw schema

Wij betalen onszelf per project en gebruiken OSP als fiscale structuur om te factureren. Een deel van de inkomsten (25%) wordt gebruikt voor vaste lasten (huur, reizen, webhosting, enz.), interne werkzaamheden te ondersteunen en een financiële buffer te creëren. Aangezien de hoeveelheid en de aard van onze projecten sterk fluctueert, kunnen we niet zorgen voor een regelmatig en voldoende inkomen via opdrachten. We zijn onlangs begonnen met experimenten om een regelmatiger en voorspelbaarder inkomen te genereren met onze financiële buffer.

## Sollicitatie

Wij vragen je om je aan te melden met een voorstel, in een vorm waarvan je vindt dat die goed bij je praktijk past. Wij hopen dat je voorstel ons inzicht kan geven in de volgende vragen: 

* Wat is je huidige praktijk?
* Wat is je traject tot nu toe?
* Indien van toepassing: de onderzoekthema’s waarin je geïnteresseerd bent, en hoe je daaraan binnen OSP zou kunnen werken
* Als je solliciteert als niet-grafisch ontwerper: hoe stel je je de samenwerking met de huidige OSP-leden voor (binnen opdrachten en/of in onderzoeksprojecten)?

Indicatie van de lengte: ongeveer 1500 woorden. Elk formaat mag: pdf, website, ruw tekstbestand, video, geluid; en kan als bijlage via e-mail worden opgestuurd, of worden gehost (in dat geval stuur je ons gewoon de link).

We vragen je ook om maximaal 5 recente projecten of ervaringen op te nemen. Dit kan zowel in de vorm van een visueel portfolio als in de vorm van korte teksten waarin je verschillende projecten waaraan je hebt gewerkt toelicht.

Deze oproep werd gepubliceerd op 1 juni 2023. Reacties moeten worden gestuurd naar <cookwithus@osp.kitchen>.
De uiterste datum voor het indienen van een voorstel is 30 juni 2023.
Je ontvangt een bericht om de ontvangst van je voorstel te bevestigen.
Wij antwoorden vóór 12 juli 2023 en organiseren dan ontmoetingen in juli en september, afhankelijk van beschikbaarheid.

Als je vragen hebt over de oproep, aarzel dan niet om contact met ons op te nemen via <miam@osp.kitchen>.

## Wat we bieden

### In de studio

Deze lijst is niet uitputtend en hoort bij de complexe socio-technische omgeving die een studio is.

* Een werktafel
* Een vergadertafel met plaats voor maximaal 8 personen
* Laserprinter, kleur A3
* Een verzameling penplotters, met afdrukformaat van A ∞ → A0
* Twee Silhouette Cameo snijmachines
* Kleine thermoprinters
* A3 Scanner, beamer, papier- en kleurstalen, etc.

De studio van OSP bevindt zich op de 2e verdieping van een gebouw met liften, een van de toegangsdeuren heeft een hellingbaan. De verdieping heeft rolstoeltoegankelijke badkamers zonder leuningen. Het openbaar vervoer bevindt zich op enkele honderden meters (Kruidtuin, Rogier, Centraal Station, Brussel Congres). Het gebouw heeft fietsenrekken achter een afsluitbare deur.

### Werken met velen 

OSP bestaat voornamelijk uit Franstaligen en Nederlandstaligen, maar de meeste communicatie binnen OSP gebeurt in het Engels.
Onze studio maakt deel uit van Meyboom artist-run-spaces. De verdieping heeft een gedeelde keuken en ontmoetingsruimtes. Meyboom is gemeenschap van individuen en collectieven die op verschillende gebieden werken en die sinds de totstandkoming in 2013 voortdurend is geëvolueerd. Voorheen gevestigd in de Vaartstraat, World Trade Center 1, daarna in de voormalige hoofdzetel van Actiris aan het Beursplein; nu werken we op de tweede verdieping van het gebouw Pacheco 34, gelegen aan de Pachécolaan 34 tussen het Centraal en Noordstation in de stad Brussel – een plek die we tot een permanent adres voor onze gemeenschap willen maken.

### Kennis delen

OSP heeft **16 jaar ervaring in het maken van grafische ontwerpen met F/LOSS tools**. Onderweg hebben we vele tools uitgeprobeerd en uiteenlopende expertises verworven, waaronder:

* uitgebreide kennis van vrije software cultuur, licenties en tools;
* programmeerkennis (HTML, CSS, JS en Python);
* lettertypes ontwerpen en aanpassen;
* tools om PDF’s aan te passen: herschikken, invoegen, comprimeren en kleurvlakken verschuiven;
* (Typo)grafische en visuele expertise die rekening houdt met de productiemiddelen.

Voortdurend bijleren is belangrijk voor ons, we zien projecten als kansen om te leren van elkaars ambacht, vooral in het enorme technische ecosysteem van F/LOSS-tools.
Via de volgende links vind je enkele van onze onderzoekstrajecten: [Declarations](https://declarations.ungual.digital/), [Ecotones of collaboration](https://ecotones.caveat.be/), [Up pen down](http://osp.kitchen/live/up-pen-down/), [Drawing Curved](http://drawingcurved.osp.kitchen/foreword.xhtml).


<iframe src="https://player.vimeo.com/video/286661261?h=e887a61584" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

[Up pen down, page 5, Ductus](https://vimeo.com/286661261) 

## Huidige leden 

De huidige actieve leden zijn:

**Einar Andersen** (hij, hem) is onder meer ontwerper en ontwikkelaar. Tegenwoordig houdt Einar zich vooral bezig met webdesign en ontwikkeling, maar in zijn vrije tijd programmeert hij generatieve kunst voor plotters, onderzoekt hij alternatieve grafische workflows en vreemde toetsenborden.

**Amélie Dumont** (zij, haar) is grafisch ontwerper, typograaf en ontwikkelaar. Ze maakt tools op maat voor publicatie via webpagina’s en gedrukte dragers, en heeft ook een niet-conventionele praktijk van letterontwerp. Amélie geeft les aan erg en heeft ervaring in het geven van web-to-print workshops in kunstscholen. Ze is in september 2022 bij OSP gekomen en is nog aan het ontdekken hoe het voelt om als collectief te werken.

**Gijs de Heij** (hij, hem) is grafisch ontwerper, ontwikkelaar en kunstenaar. Binnen OSP werkt hij aan web-to-print processen en publicatieplatforms. Ook heeft hij interesse in penplotters. Binnen de onderzoeksgroep Algolit werkt hij aan algoritmische literatuur.

**Ludi Loiseau** (zij, haar) doceert typografie en digitale culturen aan [erg](https://wiki.erg.be). Ze is medeoprichter van het Belgische onderzoekstijdschrift [Médor](medor.coop) waar ze beurtelings visuele ‘piloot’, ontwerper en bestuurslid is. In 2018 was Ludi medeoprichter van het collectief [Bye Bye Binary](typotheque.genderfluid.space) dat nieuwe grafische en typografische vormen wil verkennen met inclusief schrijven als onderzoeksgebied. Ze is onlangs toegetreden tot het Poxcat-collectief dat WMXMUSIC-feesten, DJ-mixen en radioshows in Brussel ondersteunt.

**Sarah Magnan** (zij, haar) is grafisch ontwerper binnen Open Source Publishing en neemt deel aan verschillende collectieve praktijken binnen andere collectieven op het snijvlak van feminisme, dekoloniale strategieën, open source software en online kennisinstrumenten. Ze is medeoprichter van Médor en van de (irl en url) feministische vereniging Just For The Record. Ze werkt parttime op de kunstschool La Cambre om hen te helpen over te schakelen op open source tools, zorgpraktijken te faciliteren en methoden aan te dragen voor een meer diverse en inclusieve school.

**Doriane Timmermans** (zij, haar) is ontwikkelaar, ontwerper en kunstenaar met een bijzondere belangstelling voor CSS en het ambacht van ontwerpen met taal. Ze denkt graag na over systemen en geautomatiseerde processen als weloverwogen media. Ze maakt graag vreemd gevormde tools die onze manieren van doen in vraag stellen.

## Satellietleden
Verschillende leden werken af en toe mee aan projecten: Antoine Gelgon, Pierre Huyghebaert, Alexandre Leray.

<!--[faq]-->

Progressivelly filling this faq as we get questions.

### "are these memberships limited to people only living in Brussels or very nearby?"

This call is not limited to people living in Brussels, though we've noticed that it is important for us to regularly meet in the same space. We all have different rythms and involvements and several options are possible, but we do think that for a new collaboration, a minimum of 2 days a week, physically in the studio, is needed to give a shape and make space for this new relationship. However, we do understand that members might have other obligations through the year (residencies, other works) that doesn't allow them to be there physically the whole year.

In short, we prefer meeting in person and think at least two days a week would be best.

<!--[modal]-->

## Over css lijn lettertypes

We hebben deze oproep aangegrepen om te experimenteren met een oude droom: de typografie op een webpagina tekenen middels de ductus (beweging) van een lettertype, of hoofdlijn, en niet door de omtrek.

Het belangrijkste obstakel bij dit proces is het bestandsformaat. Voor een echt enkellijns lettertype (dat wil zeggen een lettertype, of vorm zonder dikte of gewicht) moet het lettertypeformaat open, niet gesloten paden ondersteunen, vrijwel geen enkel formaat doet dit. Een gesloten pad vormt een lus, denk aan de letter O. In zekere zin heeft het pad geen begin- of eindpunt. Een ongesloten, of open pad heeft uiteindes die niet aansluiten op elkaar, bijvoorbeeld de letter S.

Het is echter mogelijk om open paden in lettertypes te coderen met behulp van SVG. Dit kan via SVG-lettertypes, momenteel alleen ondersteund in Safari, of in een OpenType-lettertype doormiddel van de SVG-tabel, ondersteund door Firefox, Safari en edge, maar uitdrukkelijk afgewezen door Chrome. Er kan echter hoop zijn in dit spoor via emojii: 

> "Het SVG-lettertypeformaat - als zelfstandig lettertypeformaat - is in wezen verouderd. Vrijwel niemand gebruikt stand-alone SVG lettertypes. Maar SVG-lettertypes zijn misschien verrassend levendig, dankzij emojii. Het SVG-lettertypeformaat is eigenlijk opgenomen in OpenType, en OpenType-lettertypes kunnen een 'SVG-tabel' bevatten. Deze tabel is een lettertype in het SVG 1.1-lettertypeformaat. Lettertypes volgens deze specificatie worden ook wel 'OTF+SVG'-fonts genoemd, en zijn typisch bedoeld om ervoor te zorgen dat wanneer clientsoftware een bepaald teken in de SVG-tabel niet kan weergeven, deze terugvalt en een TTF-alternatief weergeeft. Het mooie van OTF+SVG is dat het niet alleen op lijnen gebaseerde tekens bevat, maar ook kleurinformatie - en daarom wordt dit formaat soms een "Color Font" genoemd. OTF+SVG wordt ondersteund door onder meer Photoshop, Illustrator, Firefox, MS Edge, Pages, Keynote en TextEdit. Hierdoor bestaan SVG-lettertypen nog steeds, ook al worden ze niet vaak op zichzelf waargenomen."
>
> &mdash; uit [hershey-text-v30](https://www.evilmadscientist.com/2019/hershey-text-v30/)

Deze methode werd onderzocht door isdaT in hun [SingleLine_otf-svgMaker](https://github.com/isdat-type/SingleLine_otf-svgMaker).

> "De resulterende OpenType-SVG lettertypes met open paden bestaan uit twee lagen: een enkellijns laag gebaseerd op SVG en een tweede klassieke OTF laag waarin de letteromtrekken worden beschreven. Als een programma de SVG-laag met open paden niet ondersteunt wordt de OTF-laag gebruikt, die ook in deze OpenType-SVG-lettertypes is ingesloten."

## Het ambacht

Wat er op deze pagina gebeurt, is anders. 

Het maakt gebruik van een hack, ontwikkeld door mensen die lijnlettertypes gebruiken voor cnc- en lasersnijmachines en hun lettertypes online uitwisselen. Hun truc is om open paden om te zetten in **kunstmatig gesloten paden**. Dit gebeurt door het pad te verdubbelen, de richting van de kopie om te draaien en dan het oorspronkelijke pad en zijn omgekeerde kopie te verbinden. Dit resulteert in een pad waarvan elk punt verdubbeld is, behalve de uiteinden. Hiermee kan een dubbellijns lettertype zonder dikte worden geëxporteerd, dat zich meestal gedraagt als een echt enkellijns lettertype door de exacte superpositie van de twee paden.

Voor deze website hebben we een [python script](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf) gebruikt, met de [python fontforge module](https://fontforge.org/docs/scripting/python.html), om een **enkellijns SVG lettertype** te converteren naar een **dubbellijns OTF lettertype**.

Gelukkig bestaan er al vrije echte enkellijns SVG-lettertypen dankzij de populariteit ervan in CNC-toepassingen. Er is de inkscape extensie **Hershey text**, voor plottertekeningen, die meerdere varianten bevat van de **Hershey** lettertypes, die ook op deze pagina gebruikt worden.

## CSS

Zodra het lettertype is gegenereerd, wordt de daadwerkelijke lijn getekend in de browser zelf met CSS met behulp van de [`-webkit-tekst-stroke`](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke) eigenschap.

Deze eigenschap wordt ondersteund door de meeste browsers, en is opgenomen in de [Living standard](https://compat.spec.whatwg.org/#the-webkit-text-stroke), maar nog geen onderdeel van een CSS standaard. Dit impliceert een grote kwetsbaarheid, waardoor het onbruikbaar is voor breed toegankelijke webpagina's. We hebben dit aangegrepen om het experiment uit te breiden met een **lijn | omtrek** schakelaar, een gelegenheid om het verschil in processen te laten zien.

Deze bijzondere aanpak heeft echter andere interessante gevolgen, waarvan liefhebbers van lijnlettertypen misschien hadden gedroomd om ze op het web mogelijk te maken.

Omdat `-webkit-text-stroke` **globale eenheden** ondersteunt (d.w.z. eenheden onafhankelijk van lettergrootte), kunnen we:

* dezelfde lijndikte gebruiken voor tekstelementen met verschillende `font-size` (aangezien de lijndikte onafhankelijk is van de lettergrootte)
* dezelfde lijdikte gebruiken voor verschillende lettertypes
* dezelfde lijndikte gebruiken als andere HTML of SVG elementen (een rand van een `div`, de omtrek van een SVG tekening, etc...)
* er bestaan al meerdere vrije lettertypes die hiervoor toegankelijk zijn op het web: echte enkellijns SVG lettertypes uit CNC softwares.

## Kwetsbaarheid

Dit experiment kan gezien worden als een kruising van meerdere interesses van OSP, zoals:

* De stroke versus outline typografie paradigma's
* CSS en de korrel van het web mediums
* Fricties met open standaarden
* Hoe tools praktijken vormen en beïnvloeden






