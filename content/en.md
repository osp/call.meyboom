
<!--[call]-->

[TOC]

# OSP call for members

We, Open Source Publishing (OSP), are looking for people **to join our collective**.

[OSP](https://osp.kitchen) is a graphic design collective based in Brussels. We work on typography, websites, web-to print tools and plotters. We exclusively use free and open source software (F/LOSS). Through our projects we question software as cultural objects and modes of collaboration between graphic designers, artists, cultural institutions and schools. We question the influence and affordances of digital tools through the practice of (commissioned) graphic design, teaching and applied research.

![A vintage looking picture of all the collective members: (from left to right) Amélie, Sarah, Ludi, Einar, Doriane, Gijs.](media/us-2.jpg)

<!-- We've also published a call for residencies within OSP [here]().                                      -->

<!-- (tools and interfaces) -->

## Current status

We are currently **increasing the role of research based projects**, shifting from a practice that is mostly commission based. As part of this shift, we would like to welcome new members with their own expertise and experiences. We want to make space for researchers, artists, designers, students and/or amateurs who have an interest in our practice and would like to connect their own to it.

Within OSP the members are currently working on:

* collaborative publishing platforms;
* web-to-print publication processes;
* stroke fonts and licenses;
* specificities of web standards (HTML/CSS) as a medium.

We do this through commissions, research and teaching. 
Outside of OSP our practices also include algorithmic literature, inclusive typography, cyber feminism, textile coding, and dj-ing. We collaborate with: cultural institutions, curators, artists, architects, journalists and researchers.
We design and develop websites, books and identities, and organise workshops. From time to time we are solicited for interviews or contributions as researchers because of our singular experience with open-source tools and culture, and conscious relationship with the tools we use.

OSP as a collectivity has jagged edges. Almost all members have a practice next to their work within OSP, the involvement ranges from one to three days a week.  <!--Next to it we have our own practice in design, code and education. -->
With this call we want to make the process of joining or collaborating with OSP more transparent.

## Call for members

We are looking for **two new members who can bring new experiences and practices**. That is to say, we would like to be surprised, so you are welcome to apply even if you’re not a graphic designer or developer. However, questions around free culture, digital design, interfaces or tools can be a common denominator. Your involvement in OSP can take different shapes depending on your wishes and needs. <!-- For example, none of the current members is full time involved in the collective. -->

We are in a process of diversifying and investigating how research can live alongside our daily OSP practices. That's why we’re particularly interested in people who are experienced in facilitating this type of process or exploring new ways of working together on similarly diverse practices.<!-- When the occasion is there, members can join force to apply for subsidies for specific research projects.-->
The collaborative aspect of our work is fundamental to us, and a big part of our practice. Projects often come in through the individual members. We welcome you to bring your own practice and ecosystem to extend the collective.

<!--In our statutes the forms of memberships are described more precisely. 
With this call we are looking for “effective members”. In the statutes we list what we expect from an “effective member”:-->

Summing up, we are looking for members who:

* demonstrate an ability to work collaboratively (not work alone);
* show an ability to expand and deepen their practice through research (artistic or academic);
* use free and open source software whenever possible;
* publish source files under free licenses;
<!-- * have read: OSP asbl statutes, [OSP asbl collaboration agreement](#) and [Recipes of internal order](#) till the end. -->

Concretely we imagine you to:

* join us on commisions, and/or research projects;
* actively contribute to OSP as a collective, notably by participating to weekly meetings;
* work in our studio space according to your schedule

We pay ourselves on a per-project basis, using OSP as a fiscal structure to invoice. A part of the income (25%) is used to pay for costs (rent, travel, webhosting, etc.), to support internal work and to create a financial buffer. 
As the flow and nature of OSP projects fluctuates a lot, we can not provide a regular, and sufficient income through commisions. We’ve recently started experiments with providing a more regular and predictable income through our financial buffer.


## Application process

We ask you to apply with a proposal, in any form that you think fits you practice well.
We hope your proposal can give us insight to the following questions: 

* What is your **current practice**?
* What has been your **trajectory** so far?
* If applicable: the **tracks of research** you’re interested in and ideas on how to pursue them within OSP.
* If you apply as a non-graphic-designer: precise how you **imagine the collaboration** with the current OSP members (on commission and/or in research projects)?

length indication: around 1500 words.
It can be in any format you prefer: pdf, website, raw text file, video, sound; and can be either sent a joint piece through email, or hosted (in which case simply send us the link).

We also ask you to include maximum 5 recent projects or experiences.
This can take the form of a visual portfolio as well as short texts explaining different projects you’ve worked on.

This call was first published on June 1<sup>st</sup> 2023.
Applications should be sent to <cookwithus@osp.kitchen>.
The deadline for proposals is June 30<sup>th</sup> 2023.
You will receive an automatic reply to confirm receipt. 
We will get back to you by July 12<sup>th</sup>, and will then organize meetings in July and September according to your availability.

If you have any questions about the application, do not hesitate to contact us at <miam@osp.kitchen>.

## What we offer

### At the studio

This list is non-exhaustive and come with the complex socio-technical environment that is a studio.

* A Desk
* A meeting table with space for up to 8 people
* Laser printer, color A3
* A collection of pen plotters, with print size ranging from A&nbsp;∞ → A0
* Two Silhouette Cameo cutting machines
* Small thermo printers
* A3 Scanner, beamer, paper and color samples, etc.


![A vintage looking picture of the building where the current studio is, the Meyboom.](media/meyboom.jpg)

The studio of OSP is located on the 2nd floor of a building equipped with elevators, one of the access doors has a ramp. The floor has wheelchair accessible bathrooms without handrails. Public transport is a few hundred meters away (Botanique/Kruidtuin, Rogier, Central station, Brussels Congres). The building has bike racks behind a lockable door.

<!-- ![Looking at the North tower](media/window-view.jpg) -->

### Working with many

OSP is composed mostly of French and Dutch speakers, but most of communication within OSP is done in English.

Notably, our studio is part of [Meyboom artist-run-spaces](http://meyboom.osp.kitchen). The floor has a shared kitchen and meeting spaces. Meyboom brings together a community of individuals and collectives working in different fields that has continually evolved since its formation in the year 2013. Formerly located in Vaartstraat, World Trade Center 1, then in the previous headquarters of Actiris in the city center; we are now inhabiting the second level of the building Pacheco 34, located on Boulevard Pachécolaan 34 between the central and north station in the city of Brussels — a place we want to make a permanent address for our community.

### Sharing knowledge

OSP has **16 years of practice** in making graphic design **with F/LOSS tools**.
On the way we’ve tried many tools and gained a diverse collection of expertises, including:

* extensive knowledge on free software culture, licenses and tools;
* programming knowledge (HTML, CSS, JS and Python) and tool development;
* font bending and drawing;
* tools to bend PDFs: reorder, impose, compress and shift color space;
* (Typo)graphic and visual expertise that takes into account the means of production.

Constant relearning is important to us, and we see projects as opportunities to learn from the fragile crafts of each other, especially in the vast technical ecosystem of F/LOSS tools.

Through the following links you'll find some of our research:
[Declarations](https://declarations.ungual.digital/), [Ecotones of collaboration](https://ecotones.caveat.be/), [Up pen down](http://osp.kitchen/live/up-pen-down/), [Drawing Curved](http://drawingcurved.osp.kitchen/foreword.xhtml).


<iframe src="https://player.vimeo.com/video/286661261?h=e887a61584" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

[Up pen down, page 5, Ductus](https://vimeo.com/286661261)         

## Current members

The current members active on a daily basis are:

**Einar Andersen** (he, him) is a designer, developer among other things. These days Einar mostly works with web design and development but spends his freetime programming generative art for plotters, exploring alternative graphic design workflows and weird keyboards.

**Amélie Dumont** (she, her) is a graphic designer, typographer and developer. She cooks custom tools for publishing through webpages and print supports and also has a non-conventional type design practice. Amélie is also a teacher at erg and has experience in giving web-to-print workshops in art schools. She joined OSP in September 2022 and is still discovering how it feels to work as a collective.

**Gijs de Heij** (he, him) is graphic designer, developer and artist. Within OSP he works on web-to-print processes and publishing platforms. Also he has an interest in pen plotters. Within the research group Algolit he works on algorithmic literature.

**Ludi Loiseau** (she, her) teaches typography and digital cultures at [erg](https://wiki.erg.be). She is co-founder of the Belgian investigative magazine [Médor](medor.coop) where she is in turn visual pilot, page designer and board member. In 2018 Ludi co-initiated the collective [Bye Bye Binary](typotheque.genderfluid.space) which proposes to explore new graphic and typographic forms taking inclusive writing as a research field. She recently joined Poxcat collective who supports WMXMUSIC Parties, DJ mixes & Radio Shows in Brussels.

**Sarah Magnan** (she, her) is a graphic designer in Open Source Publishing and takes part in several collective practices within other collectives at the intersection between feminism, decolonial strategies, open source softwares and online knowledge tools. She is co-founder of Médor and of the (irl and url) feminist asbl Just For The Record. She works part time at La Cambre art school, helping to shift to open source tools, facilitating care practices, proposing methods for a more diverse and inclusive school. 

**Doriane Timmermans** (she, her) is a developer, designer and artist, with a particular interest in CSS and the crafts of designing with language.
She likes to think about systems and automatised processes as sensible media. She loves to create weirdly shaped tools that question our ways of doing.

## Satellite members

Several members join us occasionally on projects : Antoine Gelgon, Pierre Huyghebaert, Alexandre Leray.


<!--[faq]-->

Progressivelly filling this faq as we get questions.

### "are these memberships limited to people only living in Brussels or very nearby?"

This call is not limited to people living in Brussels, though we've noticed that it is important for us to regularly meet in the same space. We all have different rythms and involvements and several options are possible, but we do think that for a new collaboration, a minimum of 2 days a week, physically in the studio, is needed to give a shape and make space for this new relationship. However, we do understand that members might have other obligations through the year (residencies, other works) that doesn't allow them to be there physically the whole year.

In short, we prefer meeting in person and think at least two days a week would be best.



<!--[modal]-->

## A word on css-rendered stroke fonts

We took the opportunity of this call to experiment a long dreamt idea:
to draw the typography on a webpage by the ductus of a font, or main stroke, and not by its outline.

The main obstacle in this process is one of file format. For a real single-line font (a font, or shape without thickness or weight) the fontformat should support open, unclosed paths, virtually no fontformats supports those. A closed path forms a loop, e.g. the letter O. In a way the path doesn't have a begin- nor endpoint. An unclosed, or open path has extremities which don't join, e.g. the letter S.

It is possible to encode open paths in fonts using SVG. This can be done through SVG fonts, currently only supported in Safari, or, through the SVG table in an OpenType font, supported by Firefox, Safari and edge, but explicitely rejected by Chrome. However there may be hope in this track through emojii: 

> “The SVG font format — as a stand-alone font format — is essentially obsolete. More or less, no one is using stand-alone SVG fonts. However, SVG fonts are perhaps surprisingly alive, thanks to emoji. The SVG font format has actually been enshrined within OpenType, and OpenType fonts can contain an ‘SVG font table’, which is simply a font constructed according to the SVG 1.1 font format. Fonts of this spec are called ‘OTF+SVG’ fonts, and typically intend that when client software cannot display a given character in the SVG table, it will fall back and display a TTF alternative. The neat thing about OTF+SVG is that in addition to having stroke-based characters, it can also contain color information — and so this format is sometimes referred to as a ‘Color Font’. OTF+SVG is supported by Photoshop, Illustrator, Firefox, MS Edge, Pages, Keynote, TextEdit, and others. Because of this, SVG fonts do still exist, even though they aren’t often observed on their own.”
>
> &mdash; from [hershey-text-v30](https://www.evilmadscientist.com/2019/hershey-text-v30/)

This approach was explored by isdaT in their [SingleLine_otf-svgMaker](https://github.com/isdat-type/SingleLine_otf-svgMaker).

> “The resulting open paths OpenType-SVG fonts comprise two layers: a single-line layer based on SVG and a second layer with a classic outlined OTF. By default, if an application can’t use the open paths SVG layer it should switch automatically to the outlined OTF layer also embedded in this OpenType-SVG fonts.”

## The craft

What is happening on this page, is different. 

It uses a hack develped by people using stroke fonts for cnc and laser cutting machines, who have been exchanging online fonts made through this hack for years. Their trick is to transform open paths into **artificialy closed paths**. This is done by doubling the path, reversing the direction of the copy and then connecting the orginal path and its reversed copy. This results in a path where every point is doubled, except for the extremities. It allows to export a zero-thickness double-line font, which mostly behaves like a true single-line font because of the exact superposition of the two paths.

For this website we used a [python script](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf), using the [python fontforge module](https://fontforge.org/docs/scripting/python.html), to convert any **true single-line SVG font** to a **double-line OTF**, allowing an experimental use of “single-line” fonts on the web.

Fortunately, libre true single-line SVG fonts already exist, thanks to CNC uses. 
There is notably the inkscape extension **Hershey text**, for plotter drawings, which contains multiple variants of the **Hershey** fonts, used here. Other examples can be found on fablab forums and guides.

## CSS

Once the font is generated, the actual stroke is drawn in the browser itself with CSS using the [`-webkit-text-stroke`](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke) property.

This property is supported by most browsers, and included in the [Living standard](https://compat.spec.whatwg.org/#the-webkit-text-stroke), but not part of a CSS standard yet. This implies a great fragility, making it unusable for well-accessible webpages. We took this as an opportunity to extend the experiment with a **stroke | outline** toggle, an occasion to unfold and show the difference of processes. Note that, more specifically:

* Chrome renders square terminals and Firefox does rounded one, leaving the interpretation of the stroke itself to the browser.
* some letters may disappears, and some curves can appears as doubled at certain zoom level only, we have no idea why, but of course `-webkit-text-stroke` was never thought for that usage.

This particular approach has different nice consequences, that stroke font enjoyers may have dreamt to see possible on the web.

Because `-webkit-text-stroke` supports **global units** (meaning units independent to font-size), we can:

* make the same stroke width for text elements with different `font-size` (as the stroke width become independent from the font-size)
* make the same stroke width for different font-famillies
* make the same stroke width with other HTML or SVG elements (a border of a `div`, the outline of an SVG drawing, etc...)
* there already exist multiple libre font that can be processed for this use on the web: true single-lines SVG font from the libre CNC softwares worlds.

## Fragility

This experiment could be seen as crossing multiple fields of interest of OSP, such as:

* The stroke versus outline typography paradigms
* CSS and the grain of the web mediums
* Frictions with open standards
* How tools shape practices
