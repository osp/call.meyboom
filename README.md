# call.meyboom

a webpage for the OSP member call.

To generate the website, run:

        python3 make.py

The content is in markdown in the folder `content/`.

To install requirements, run:

        pip install -r requirements.txt

## upload

to upload make an `.env` file containing:

```
SSH_HOST="osp.kitchen"
SSH_LOCATION="/srv/www/kitchen.osp.call"
SSH_USERNAME="username"
SSH_PASSWORD="password"
```

and, subsequently run:

        ./scp-sync.sh

