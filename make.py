import jinja2
import os

from markdown import markdown
import yaml
import json

# for date and time filtering
import datetime
from dateutil.parser import *

# for media copies
from distutils.dir_util import copy_tree

# for TOC extraction
from bs4 import BeautifulSoup

# for custom markdown comment-sections
import re

# for pads imports
import requests


# TODO:
# implement the io=f2f or io=f2o mode


# OPTIONS
# ------------------------------------------------------------------

CONTENT_PATH = 'content/'
TEMPLATE_PATH = 'template.html'
HTML_PATH = 'www/'
MEDIA_EXTENSION = '.media'

options = {
    'url': 'https://call.osp.kitchen',
    'title': 'OSP Member Call',
    'description': 'an invitation for new members to join our collective Open Source Publishing', 
    'print': False,
    'reset': False,
    # every file produce one file
    'multiple': True,
    'TOC': True
}


# MARKDOWN EXT
# ------------------------------------------------------------------

# https://python-markdown.github.io/extensions/extra/ 
# (official support for: fenced code block, footnotes)
from markdown.extensions.extra import ExtraExtension

# https://python-markdown.github.io/extensions/toc/
# (official support for TOC)
from markdown.extensions.toc import TocExtension

# https://python-markdown.github.io/extensions/code_hilite/
# (official support for syntax highlighting)
# from markdown.extensions.codehilite import CodeHiliteExtension

# https://github.com/funk1d/markdown-figcap
# (third party for figcaption, allow complexe ones with html in it and custom figure content)
# from markdown_figcap import FigCapExtension

# https://github.com/evidlo/markdown_captions
# (third party for figcaption, simply transform img alt text into caption)
# NOTE: figure still wrapped in <p>
# from markdown_captions import CaptionsExtension

# https://pypi.org/project/markdown3-newtab/
# (third party that make every link into a new tab link)
# NOTE: create a link typology extension
# from markdown3_newtab import NewTabExtension

from yafg import YafgExtension

MARKDOWN = {
    'extensions': [
        YafgExtension(stripTitle=False),
        ExtraExtension(),
        # CodeHiliteExtension(),
        # FigCapExtension(),
        # FootnoteExtension(),
        TocExtension()
    ],
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'codelight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.footnotes': {},
    }
}


# PARSING
# ------------------------------------------------------------------

def read_file(NAME_PATH):
    txt = ""
    with open(NAME_PATH, 'r') as file:
        txt = file.read()
    return txt

def md_parse(txt):
    md = ""
    md = markdown(txt, 
    extensions=MARKDOWN['extensions'], 
    extension_configs=MARKDOWN['extension_configs'])
    return md


def yml_parse(txt):
    yml = ""
    yml = yaml.safe_load(txt)
    return yml


def split_sections(page):
    """
    Split the markdown content of an article or page into sections
    everytime it encounter a comment of the form <!--[section-name]--> 
    and put those in a new `sections` attribute as a dictonnary.

    The `content` attribute is left intact.
    """

    sections = []

    pattern = re.compile(r"<!--\[((?:(?!\]-->).)*)\]-->((?:(?!<!--\[)[\s\S])+)")

    # find all matches to groups
    matches = pattern.finditer(page['txt'])

    for match in matches:
        # section name
        section_name = match.group(1)
        section_content = match.group(2)
        page[section_name] = section_content
        sections.append(section_name)

    if sections:
        sections_names = ", ".join(sections)
        print("Sections: {s}".format(s=sections_names))


# COLLECTING CONTENT
# ------------------------------------------------------------------

def collect_content():

    # filling a content dictionnary from all the .md or .yml file in the content folder
    possible_extensions = ['.md', '.yml', '.yaml']
    content = {}

    for root, dirs, files in os.walk(CONTENT_PATH):
        for filename in files:
            (basename, ext) = os.path.splitext(filename)
            path = os.path.join(root, filename)
            if ext in possible_extensions:

                print("{f} parsing...".format(f = filename))
                
                # items have basename as 'id'
                content[basename] = {
                    'txt': "",
                    'media': []
                }

                # parsing content to 'text' key
                if ext == ".md":

                    content[basename]['txt'] = read_file(path)
                    lines = content[basename]['txt'].split('\n')

                    # is a pad request
                    if lines[0] == "!":
                        pad_url = lines[1] + '/export/txt'
                        pad = requests.get(pad_url)
                        print(pad_url)
                        content[basename]['txt'] = md_parse(pad.text)
                    
                    # splitting sections
                    split_sections(content[basename])

                    for section_name, section in content[basename].items():
                        if section_name not in ['txt', 'media']:
                            content[basename][section_name] = md_parse(section)

                # extracting TOC to a 'toc' value
                if options['TOC']:
                    soup = BeautifulSoup(content[basename]['call'], 'html.parser')
                    find_toc = soup.find("div", class_="toc")
                    if find_toc:
                        toc = find_toc.extract()
                        content[basename]['toc'] = toc
                        content[basename]['call'] = str(soup)

                # parsing media to 'media' key
                content[basename]['media'] = collect_media(filename)

                print("{f} parsed".format(f = filename))
    
    return content


# COLLECTING MEDIA
# ------------------------------------------------------------------

def path2mediapath(source_path):
    # convert a path of a parsed file to its corresponding media folder (ending with .media)
    filename, file_extension = os.path.splitext(source_path)
    return filename + MEDIA_EXTENSION

def collect_media(filename):

    folder_path = os.path.join("content", path2mediapath(filename))

    # stop if no media folder
    if not os.path.isdir(folder_path):
        return
    
    print('media folder: {f}'.format(f = folder_path))

    # manually copy the folder to the output directory
    save_folder_path = "media/" + path2mediapath(filename)
    copy_tree(folder_path, HTML_PATH + save_folder_path)

    # construct the urls list
    media_urls = []

    # iterate over the media folder
    for media in os.listdir(folder_path):
        if os.path.isfile(os.path.join(folder_path, media)):
            media_urls.append(os.path.join(save_folder_path, media))

    return media_urls


# MAIN
# ------------------------------------------------------------------

def write_html(template, content, path, basename = "index"):
    html = template.render(content = content, options = options, basename = basename)
    with open(path, 'w') as file:
        file.write(html)

if __name__ == '__main__':

    # collect content
    content = collect_content()

    # init jinja environment
    templateLoader = jinja2.FileSystemLoader( searchpath="" )
    env = jinja2.Environment(
        loader=templateLoader
    )

    # getting the template
    template = env.get_template(TEMPLATE_PATH)

    # render template
    if not options['multiple']:
        write_html(template, content, HTML_PATH + 'index.html')

    else:
        for basename, article in content.items():
            write_html(template, content, HTML_PATH +  basename + '.html', basename)